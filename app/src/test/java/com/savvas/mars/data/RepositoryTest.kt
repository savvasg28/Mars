package com.savvas.mars.data

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.savvas.mars.data.remote.RemoteDataSource
import com.savvas.mars.domain.Repository
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class RepositoryTest {

    lateinit var remoteDataSource: RemoteDataSource
    lateinit var repository: Repository

    @Before
    fun setUp(){
        remoteDataSource = mock()
        repository = RepositoryImp(remoteDataSource)
    }

    @Test
    fun getMarsListCompletes(){
        whenever(remoteDataSource.getMarsItems(1)).doReturn(Single.just(listOf()))
        val testObserver = repository.getMarsItems().test()
        testObserver.assertComplete()
    }

}