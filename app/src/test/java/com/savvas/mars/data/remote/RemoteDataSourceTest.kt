package com.savvas.mars.data.remote

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import com.savvas.mars.data.remote.client.NasaAPI
import com.savvas.mars.data.remote.converter.RemoteEntityConverter
import com.savvas.mars.data.remote.model.MarsItemResponse
import com.savvas.mars.domain.entity.MarsEntity
import com.savvas.mars.utils.MarsDataFactory
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
class RemoteDataSourceTest {

    lateinit var nasaAPI: NasaAPI
    lateinit var remoteEntityConverter: RemoteEntityConverter
    lateinit var remoteDataSource: RemoteDataSource
    var pageNumber: Int  = 1

    @Before
    fun setUp(){
        nasaAPI = mock()
        remoteEntityConverter = RemoteEntityConverter()
        remoteDataSource = RemoteDataSource(nasaAPI,remoteEntityConverter)
    }


    @Test
    fun `Get Mars List Completes`(){
        stubNasaApi(Single.just(MarsDataFactory.provideMarsListResponse()))
        val testObserver = remoteDataSource.getMarsItems(pageNumber).test()
        testObserver.assertComplete()
    }

    @Test
    fun `Reads Mars List Data`(){
        val marsItemsResponse = MarsDataFactory.provideMarsListResponse()
        stubNasaApi(Single.just(marsItemsResponse))
        val marsListEntity = mutableListOf<MarsEntity>()
        marsItemsResponse.collection.items.forEach{
            marsListEntity.add(remoteEntityConverter.invoke(it))
        }
        val testObserver = remoteDataSource.getMarsItems(pageNumber).test()
        testObserver.assertValue(marsListEntity)
    }


    private fun stubNasaApi(observer:Single<MarsItemResponse>) =
        whenever(nasaAPI.getMarsItems()).doReturn(observer)
}