package com.savvas.mars.data.remote.converter

import com.savvas.mars.domain.entity.MarsEntity
import com.savvas.mars.utils.MarsDataFactory
import junit.framework.TestCase.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4


@RunWith(JUnit4::class)
class RemoteEntityConverterTest {

    lateinit var remoteEntityConverter: RemoteEntityConverter

    @Before
    fun setUp(){
        remoteEntityConverter = RemoteEntityConverter()
    }

    @Test
    fun `Successful Conversion`(){
        val marsItem = MarsDataFactory.provideMarsResponse().collection.items[0]
        val marsEntity = remoteEntityConverter.invoke(marsItem)
        assertMarsEntityEquality(marsEntity, MarsDataFactory.provideMarsEntity())
    }


    private fun assertMarsEntityEquality(m1: MarsEntity,m2: MarsEntity) {
        assertEquals(m1.id,m2.id)
        assertEquals(m1.imageSrc,m2.imageSrc)
        assertEquals(m1.description,m2.description)
    }
}