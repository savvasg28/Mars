package com.savvas.mars.domain

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.savvas.mars.core.schedulers.BaseSchedulerProvider
import com.savvas.mars.core.schedulers.ImmediateSchedulerProvider
import com.savvas.mars.domain.entity.MarsEntity
import com.savvas.mars.utils.MarsDataFactory
import io.reactivex.Single
import org.junit.Before
import org.junit.Test


class GetMarsDetailUseCaseTest {

    lateinit var repository: Repository
    lateinit var schedulerProvider: BaseSchedulerProvider
    lateinit var getMarsItemUseCase: GetMarsDetailUseCase

    @Before
    fun setUp() {
        repository = mock()
        schedulerProvider = ImmediateSchedulerProvider()
        getMarsItemUseCase = GetMarsDetailUseCase(repository, schedulerProvider)
    }

    @Test
    fun `Calls repository`() {
        stubRepository(Single.just(MarsDataFactory.provideMarsEntity()))
        getMarsItemUseCase.observe(MarsDataFactory.nasaId)
        verify(repository).getMarsItem(MarsDataFactory.nasaId)
    }

    @Test
    fun `Get Mars List Completes`() {
        stubRepository(Single.just(MarsDataFactory.provideMarsEntity()))
        val testObserver = getMarsItemUseCase.observe(MarsDataFactory.nasaId).test()
        testObserver.assertComplete()
    }

    private fun stubRepository(observer: Single<MarsEntity>) =
        whenever(repository.getMarsItem(MarsDataFactory.nasaId)).doReturn(observer)
}