package com.savvas.mars.domain

import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import com.savvas.mars.core.schedulers.BaseSchedulerProvider
import com.savvas.mars.core.schedulers.ImmediateSchedulerProvider
import com.savvas.mars.utils.MarsDataFactory
import io.reactivex.Single
import org.junit.Before
import org.junit.Test


class GetMarsListUseCaseTest {

    lateinit var repository: Repository
    lateinit var schedulerProvider: BaseSchedulerProvider
    lateinit var getMarsListUseCase: GetMarsListUseCase

    @Before
    fun setUp() {
        repository = mock()
        schedulerProvider = ImmediateSchedulerProvider()
        getMarsListUseCase = GetMarsListUseCase(repository, schedulerProvider)
    }

    @Test
    fun `Calls repository`() {
        stubRepository(Single.just(MarsDataFactory.provideListMarsEntity(1)))
        getMarsListUseCase.observe()
        verify(repository).getMarsItems()
    }

    @Test
    fun `Get Mars List Completes`() {
        stubRepository(Single.just(MarsDataFactory.provideListMarsEntity(1)))
        val testObserver = getMarsListUseCase.observe().test()
        testObserver.assertComplete()
    }

    private fun stubRepository(observer: Single<MarsList>) =
        whenever(repository.getMarsItems()).doReturn(observer)
}