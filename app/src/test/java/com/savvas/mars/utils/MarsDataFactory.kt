package com.savvas.mars.utils

import com.google.gson.Gson
import com.savvas.mars.data.remote.model.MarsItemResponse
import com.savvas.mars.domain.entity.MarsEntity


object MarsDataFactory {

    val nasaId = "NHQ201905310040"

    fun provideMarsEntity() =
        MarsEntity(
            id = "NHQ201905310040",
            imageSrc = "https://images-assets.nasa.gov/image/NHQ201905310040/NHQ201905310040~thumb.jpg",
            description = "The Mars celebration Friday, May 31, 2019, in Mars, Pennsylvania. NASA is in the small town to celebrate Mars exploration and share the agency’s excitement about landing astronauts on the Moon in five years. The celebration includes a weekend of Science, Technology, Engineering, Arts and Mathematics (STEAM) activities. Photo Credit: (NASA/Bill Ingalls)"
        )

    fun provideListMarsEntity(count : Int) =
        mutableListOf<MarsEntity>().apply {
            repeat(count){
                add(provideMarsEntity())
            }
        }

    fun provideMarsListResponse(): MarsItemResponse = Gson().fromJson<MarsItemResponse>(marsListResponse, MarsItemResponse::class.java)

    fun provideMarsResponse(): MarsItemResponse = Gson().fromJson<MarsItemResponse>(marsSingeResponse, MarsItemResponse::class.java)

    //TODO add to JSON file
    private val marsListResponse: String = """
    {
        "collection": {
        "items": [
        {
            "data": [
            {
                "center": "HQ",
                "location": "Mars, PA, USA",
                "nasa_id": "NHQ201905310040",
                "description": "The Mars celebration Friday, May 31, 2019, in Mars, Pennsylvania. NASA is in the small town to celebrate Mars exploration and share the agency’s excitement about landing astronauts on the Moon in five years. The celebration includes a weekend of Science, Technology, Engineering, Arts and Mathematics (STEAM) activities. Photo Credit: (NASA/Bill Ingalls)",
                "photographer": "NASA/Bill Ingalls",
                "keywords": [
                "Mars",
                "Mars Celebration",
                "Pennsylvania"
                ],
                "media_type": "image",
                "date_created": "2019-05-31T00:00:00Z",
                "title": "Mars Celebration"
            }
            ],
            "href": "https://images-assets.nasa.gov/image/NHQ201905310040/collection.json",
            "links": [
            {
                "rel": "preview",
                "href": "https://images-assets.nasa.gov/image/NHQ201905310040/NHQ201905310040~thumb.jpg",
                "render": "image"
            }
            ]
        },
        {
            "data": [
            {
                "center": "HQ",
                "location": "Mars, PA, USA",
                "nasa_id": "NHQ201905310045",
                "description": "The Mars celebration Friday, May 31, 2019, in Mars, Pennsylvania. NASA is in the small town to celebrate Mars exploration and share the agency’s excitement about landing astronauts on the Moon in five years. The celebration includes a weekend of Science, Technology, Engineering, Arts and Mathematics (STEAM) activities. Photo Credit: (NASA/Bill Ingalls)",
                "photographer": "NASA/Bill Ingalls",
                "keywords": [
                "Mars",
                "Mars Celebration",
                "Pennsylvania"
                ],
                "media_type": "image",
                "date_created": "2019-05-31T00:00:00Z",
                "title": "Mars Celebration"
            }
            ],
            "href": "https://images-assets.nasa.gov/image/NHQ201905310045/collection.json",
            "links": [
            {
                "rel": "preview",
                "href": "https://images-assets.nasa.gov/image/NHQ201905310045/NHQ201905310045~thumb.jpg",
                "render": "image"
            }
            ]
        }
        ],
        "href": "https://images-api.nasa.gov/search?q=mars&page=1&media_type=image",
        "version": "1.0",
        "links": [
        {
            "rel": "next",
            "href": "https://images-api.nasa.gov/search?q=mars&media_type=image&page=2",
            "prompt": "Next"
        }
        ],
        "metadata": {
        "total_hits": 19458
    }
    }
    }
    """.trim()

    private val marsSingeResponse : String = """
        {"collection":{"href":"https://images-api.nasa.gov/search?nasa_id=NHQ201905310040","items":[{"href":"https://images-assets.nasa.gov/image/NHQ201905310040/collection.json","links":[{"render":"image","href":"https://images-assets.nasa.gov/image/NHQ201905310040/NHQ201905310040~thumb.jpg","rel":"preview"}],"data":[{"date_created":"2019-05-31T00:00:00Z","photographer":"NASA/Bill Ingalls","nasa_id":"NHQ201905310040","location":"Mars, PA, USA","description":"The Mars celebration Friday, May 31, 2019, in Mars, Pennsylvania. NASA is in the small town to celebrate Mars exploration and share the agency’s excitement about landing astronauts on the Moon in five years. The celebration includes a weekend of Science, Technology, Engineering, Arts and Mathematics (STEAM) activities. Photo Credit: (NASA/Bill Ingalls)","title":"Mars Celebration","media_type":"image","keywords":["Mars","Mars Celebration","Pennsylvania"],"center":"HQ"}]}],"version":"1.0","metadata":{"total_hits":1}}}
        """.trim()
}
