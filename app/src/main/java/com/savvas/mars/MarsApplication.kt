package com.savvas.mars

import android.app.Application
import androidx.annotation.UiThread
import com.savvas.mars.core.di.AppModule
import com.savvas.mars.core.di.DaggerMarsAppComponent
import com.savvas.mars.core.di.MarsAppComponent
import com.savvas.mars.data.di.DataModule
import com.savvas.mars.data.di.RemoteDataModule
import timber.log.Timber


class MarsApplication : Application() {

    companion object{
        lateinit var appComponent: MarsAppComponent
    }

    override fun onCreate() {
        super.onCreate()
        if(BuildConfig.DEBUG) Timber.plant(Timber.DebugTree())
        appComponent = getAppComponent()
    }

    @UiThread
    private fun getAppComponent() =
        DaggerMarsAppComponent.builder()
            .appModule(AppModule(applicationContext))
            .dataModule(DataModule())
            .remoteDataModule(RemoteDataModule())
            .build()
}