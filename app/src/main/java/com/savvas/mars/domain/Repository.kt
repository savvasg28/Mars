package com.savvas.mars.domain

import com.savvas.mars.domain.entity.MarsEntity
import io.reactivex.Single


interface Repository {

    fun getMarsItems() : Single<MarsList>

    fun getMarsItem(nasaId: String) : Single<MarsEntity>
}