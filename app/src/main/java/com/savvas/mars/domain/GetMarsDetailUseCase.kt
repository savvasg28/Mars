package com.savvas.mars.domain

import com.savvas.mars.core.domain.SingleUseCase
import com.savvas.mars.core.schedulers.BaseSchedulerProvider
import com.savvas.mars.domain.entity.MarsEntity
import io.reactivex.Single


class GetMarsDetailUseCase(
    private val repository: Repository,
    schedulerProvider: BaseSchedulerProvider
) : SingleUseCase<MarsEntity, String>(schedulerProvider) {

    override fun build(params: String?): Single<MarsEntity> =
        repository.getMarsItem(params!!)
}