package com.savvas.mars.domain.error


sealed class ErrorEntity {

    abstract val exception: Throwable

    data class Network(override val exception: Throwable) : ErrorEntity()

    data class BadRequest(override val exception: Throwable) : ErrorEntity()

    data class ServerError(override val exception: Throwable) : ErrorEntity()

    data class NotFound(override val exception: Throwable) : ErrorEntity()

    data class Unknown(override val exception: Throwable) : ErrorEntity()
}