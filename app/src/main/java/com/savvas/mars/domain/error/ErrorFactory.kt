package com.savvas.mars.domain.error

import com.savvas.mars.domain.error.ErrorEntity.*
import retrofit2.HttpException
import java.io.IOException
import java.net.HttpURLConnection.*

/**
 * Error Factory class that generates error entity from exceptions based on
 * Api documentation from NASA see .....
 */
class ErrorFactory {

    fun create(throwable: Throwable): ErrorEntity {

        return when (throwable) {
            is IOException -> Network(throwable)

            is HttpException -> {
                when (throwable.code()) {

                    HTTP_NOT_FOUND -> NotFound(throwable)

                    HTTP_BAD_REQUEST -> BadRequest(throwable)

                    HTTP_UNAVAILABLE -> ServerError(throwable)

                    HTTP_INTERNAL_ERROR -> ServerError(throwable)

                    HTTP_BAD_GATEWAY -> ServerError(throwable)

                    else -> Network(throwable)
                }
            }

            else -> Unknown(throwable)
        }
    }
}