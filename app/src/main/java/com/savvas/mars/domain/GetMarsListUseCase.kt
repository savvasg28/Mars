package com.savvas.mars.domain

import com.savvas.mars.core.domain.SingleUseCase
import com.savvas.mars.core.schedulers.BaseSchedulerProvider
import com.savvas.mars.domain.entity.MarsEntity
import io.reactivex.Single

typealias MarsList = List<MarsEntity>

class GetMarsListUseCase(
    private val repository: Repository,
    schedulerProvider: BaseSchedulerProvider
) : SingleUseCase<MarsList, Int>(schedulerProvider) {

    override fun build(params: Int?): Single<MarsList> =
        repository.getMarsItems()
}