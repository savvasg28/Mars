package com.savvas.mars.domain.entity


data class MarsEntity (val id: String,
                       val imageSrc: String,
                       val description: String)