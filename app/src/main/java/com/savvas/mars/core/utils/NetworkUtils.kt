package com.savvas.mars.core.utils

import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build

@SuppressLint("ObsoleteSdkInt")
class NetworkStatus(private val context: Context) {

    fun isNetworkAvailable(): Boolean {
        var result = false
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            connectivityManager?.run {
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)?.run {
                    if ((hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) || (hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR))) {
                        result = true
                    }
                }
            }
        } else {
            connectivityManager?.run {
                @Suppress("DEPRECATION")
                connectivityManager.activeNetworkInfo?.run {
                    if ((type == ConnectivityManager.TYPE_WIFI) || (type == ConnectivityManager.TYPE_MOBILE)) {
                        result = true
                    }
                }
            }
        }
        return result

    }
}

class OfflineException(override val message: String?) : Exception()

