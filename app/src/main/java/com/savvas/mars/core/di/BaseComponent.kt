package com.savvas.mars.core.di

import android.app.Activity


interface BaseComponent<T> {

    fun inject(target: T)

}

interface BaseActivityComponent<T : Activity> : BaseComponent<T>