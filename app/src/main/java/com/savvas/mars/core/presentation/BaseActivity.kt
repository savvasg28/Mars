package com.savvas.mars.core.presentation


import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import com.savvas.mars.domain.error.ErrorEntity

abstract class BaseActivity : AppCompatActivity() {

    @get:LayoutRes
    protected abstract val layoutId: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutId)
    }

    override fun onStart() {
        super.onStart()
        bind()
    }

    abstract fun bind()


    protected fun renderProgress(isLoading: Boolean) {
        //TODO
    }

    protected fun renderError(error: ErrorEntity) {
        //TODO
    }

}
