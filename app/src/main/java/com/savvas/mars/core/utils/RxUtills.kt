package com.savvas.mars.core.utils

import com.savvas.mars.core.schedulers.BaseSchedulerProvider
import io.reactivex.Single


fun <T> Single<T>.ioui(schedulerProvider: BaseSchedulerProvider) =
    this.subscribeOn(schedulerProvider.io())
        .observeOn(schedulerProvider.ui())


//TODO add for other schedulers






