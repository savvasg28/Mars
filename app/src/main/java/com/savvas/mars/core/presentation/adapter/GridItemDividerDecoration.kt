package com.savvas.mars.core.presentation.adapter

import android.graphics.Canvas
import android.graphics.Paint
import androidx.annotation.ColorInt
import androidx.annotation.Dimension
import androidx.recyclerview.widget.RecyclerView

/**
 * Item decoration that will add the @dividerSize value to
 * the right and bottom of the grid view item
 */
class GridItemDividerDecoration(
    @param:Dimension private val dividerSize: Int,
    @ColorInt dividerColor: Int
) : RecyclerView.ItemDecoration() {

    private val paint: Paint = Paint().apply {
        color = dividerColor
        style = Paint.Style.FILL
    }

    override fun onDrawOver(canvas: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        if (parent.isAnimating) return
        val lm = parent.layoutManager ?: return

        (0 until parent.childCount).forEach { i ->
            val child = parent.getChildAt(i)

            val right = lm.getDecoratedRight(child)
            val bottom = lm.getDecoratedBottom(child)
            val left = lm.getDecoratedLeft(child)
            val top = lm.getDecoratedTop(child)

            canvas.drawRect(
                left.toFloat(),
                (bottom - dividerSize).toFloat(),
                right.toFloat(),
                bottom.toFloat(),
                paint
            )

            canvas.drawRect(
                (right - dividerSize).toFloat(),
                top.toFloat(),
                right.toFloat(),
                (bottom - dividerSize).toFloat(),
                paint
            )

        }
    }
}