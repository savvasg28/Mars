package com.savvas.mars.core.presentation

/**
 * Marker interface for view states
 */
interface BaseViewState