package com.savvas.mars.core.domain

import com.savvas.mars.core.schedulers.BaseSchedulerProvider
import com.savvas.mars.core.utils.ioui
import io.reactivex.Single

abstract class SingleUseCase<T, in Params> constructor(
    private val schedulerProvider: BaseSchedulerProvider
) {

    protected abstract fun build(params: Params? = null): Single<T>

    open fun observe(params: Params? = null): Single<T> {
        return build(params)
            .ioui(schedulerProvider)
    }
}