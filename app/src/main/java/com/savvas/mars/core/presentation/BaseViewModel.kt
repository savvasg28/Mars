package com.savvas.mars.core.presentation

import androidx.lifecycle.ViewModel
import com.savvas.mars.domain.error.ErrorFactory
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable


abstract class BaseViewModel(
    protected val disposables: CompositeDisposable,
    private val errorFactory: ErrorFactory
) : ViewModel() {


    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    protected fun handleError(throwable: Throwable) =
        errorFactory.create(throwable)

    //TODO subscribe function

}