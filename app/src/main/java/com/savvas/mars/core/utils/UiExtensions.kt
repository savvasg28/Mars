package com.savvas.mars.core.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView


fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun ProgressBar.show(isLoading: Boolean) =
    if (isLoading) this.visible() else this.gone()


fun Context.showToastLong(@StringRes message: Int) =
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()


fun Activity.hideKeyboard() {
    if (currentFocus == null) View(this) else currentFocus?.let { hideKeyboard(it) }
}

fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun RecyclerView.isScrolling() :Boolean =
    scrollState != RecyclerView.SCREEN_STATE_OFF &&
            scrollState != RecyclerView.SCROLL_STATE_IDLE


fun GridLayoutManager.setSpan(count : Int) = this.apply {
    spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
        override fun getSpanSize(position: Int): Int {
            return if (position % count == 0) 4 else 2
        }
    }
}


