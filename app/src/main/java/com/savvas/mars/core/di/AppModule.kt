package com.savvas.mars.core.di

import android.content.Context
import com.savvas.mars.core.di.scopes.ApplicationScope
import com.savvas.mars.core.schedulers.BaseSchedulerProvider
import com.savvas.mars.core.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides


@Module
class AppModule(private val context: Context) {

    @ApplicationScope
    @Provides
    fun provideContext(): Context = context.applicationContext

    @ApplicationScope
    @Provides
    fun provideSchedulers(): BaseSchedulerProvider = SchedulerProvider()
}