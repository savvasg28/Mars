package com.savvas.mars.core.di

import com.savvas.mars.core.di.scopes.ApplicationScope
import com.savvas.mars.data.di.DataModule
import com.savvas.mars.presentation.detail.di.MarsDetailComponent
import com.savvas.mars.presentation.detail.di.MarsDetailModule
import com.savvas.mars.presentation.list.di.MarsListComponent
import com.savvas.mars.presentation.list.di.MarsListModule
import dagger.Component

@ApplicationScope
@Component(modules = [DataModule::class, AppModule::class])
interface MarsAppComponent{

    fun getMarsListComponent(marsListModule: MarsListModule) : MarsListComponent

    fun getMarsDetailComponent(marsDetailModule: MarsDetailModule) : MarsDetailComponent
}