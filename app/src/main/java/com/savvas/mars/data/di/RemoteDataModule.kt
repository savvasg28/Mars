package com.savvas.mars.data.di

import com.savvas.mars.BuildConfig
import com.savvas.mars.core.di.scopes.ApplicationScope
import com.savvas.mars.data.remote.RemoteDataSource
import com.savvas.mars.data.remote.client.NasaAPI
import com.savvas.mars.data.remote.client.NasaAPI.Companion.BASE_URL
import com.savvas.mars.data.remote.converter.RemoteEntityConverter
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


@Module
class RemoteDataModule {

    @ApplicationScope
    @Provides
    fun provideRemoteDataSource(
        nasaAPI: NasaAPI,
        remoteEntityConverter: RemoteEntityConverter
    ): RemoteDataSource =
        RemoteDataSource(nasaAPI, remoteEntityConverter)

    @ApplicationScope
    @Provides
    fun provideremoteEntityConverter(): RemoteEntityConverter =
        RemoteEntityConverter()

    @Provides
    @ApplicationScope
    fun provideNasaAPI(okHttpClient: OkHttpClient): NasaAPI =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(NasaAPI::class.java)


    @Provides
    @ApplicationScope
    fun provideOkHttpClient(httpLoggingInterceptor: HttpLoggingInterceptor): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(httpLoggingInterceptor)
            .connectTimeout(15, TimeUnit.SECONDS)
            .readTimeout(15, TimeUnit.SECONDS)
            .build()

    @Provides
    @ApplicationScope
    fun provideLoggingInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().apply {
            this.level = if (BuildConfig.DEBUG)
                HttpLoggingInterceptor.Level.BODY
            else
                HttpLoggingInterceptor.Level.NONE
        }



}