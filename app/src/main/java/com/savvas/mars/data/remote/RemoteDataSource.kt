package com.savvas.mars.data.remote

import com.savvas.mars.data.DataSource
import com.savvas.mars.data.remote.client.NasaAPI
import com.savvas.mars.data.remote.converter.RemoteEntityConverter
import com.savvas.mars.domain.entity.MarsEntity
import io.reactivex.Single


open class RemoteDataSource(
    private val nasaAPI: NasaAPI,
    private val remoteEntityConverter: RemoteEntityConverter
) : DataSource {

    override fun getMarsItem(nasaId: String): Single<MarsEntity> =
        nasaAPI.getMarsItem(nasaId)
            .map { remoteEntityConverter(it.collection.items[0]) }

    override fun getMarsItems(page: Int): Single<List<MarsEntity>> =
        nasaAPI.getMarsItems(page = page)
            .map { it.collection.items }
            .toObservable()
            .flatMapIterable { it }
            .map { remoteEntityConverter(it) }
            .toList()


}