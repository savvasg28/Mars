package com.savvas.mars.data.remote.client

import com.savvas.mars.data.remote.model.MarsItemResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query


interface NasaAPI {

    @GET(SEARCH)
    fun getMarsItems(
        @Query(QUERY) query: String = MARS,
        @Query(PAGE) page: Int = 1,
        @Query(MEDIA_TYPE) mediaType: String = IMAGE
    ): Single<MarsItemResponse>

    @GET(SEARCH)
    fun getMarsItem(
        @Query(NASA_ID) nasaId: String
    ):Single<MarsItemResponse>


    companion object {
        const val BASE_URL = "https://images-api.nasa.gov"
        const val QUERY = "q"
        const val SEARCH = "search"
        const val PAGE = "page"
        const val MEDIA_TYPE = "media_type"
        const val IMAGE = "image"
        const val MARS = "Milky Way"
        const val NASA_ID = "nasa_id"
    }

}