package com.savvas.mars.data

import com.savvas.mars.domain.entity.MarsEntity
import io.reactivex.Single


interface DataSource {

    fun getMarsItems(page: Int): Single<List<MarsEntity>>

    fun getMarsItem(nasaId: String): Single<MarsEntity>


}