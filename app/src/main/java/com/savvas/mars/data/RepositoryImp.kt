package com.savvas.mars.data

import com.savvas.mars.data.remote.RemoteDataSource
import com.savvas.mars.domain.MarsList
import com.savvas.mars.domain.Repository
import com.savvas.mars.domain.entity.MarsEntity
import io.reactivex.Single


class RepositoryImp(private val remoteDataSource: RemoteDataSource) : Repository {

    override fun getMarsItems(): Single<MarsList> =
        remoteDataSource.getMarsItems(1)

    override fun getMarsItem(nasaId: String): Single<MarsEntity> =
        remoteDataSource.getMarsItem(nasaId)
}