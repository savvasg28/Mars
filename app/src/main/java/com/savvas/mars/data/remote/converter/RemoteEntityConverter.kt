package com.savvas.mars.data.remote.converter

import com.savvas.mars.data.remote.model.MarsItem
import com.savvas.mars.domain.entity.MarsEntity


class RemoteEntityConverter : (MarsItem) -> MarsEntity {

    override fun invoke(p1: MarsItem): MarsEntity =
            with(p1.data[0]) {
                MarsEntity(
                    nasa_id,
                    p1.links[0].href,
                    description
                )
            }
}