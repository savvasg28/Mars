package com.savvas.mars.data.remote.model

data class MarsItemResponse(var collection: NasaCollection)

data class NasaCollection(val items: List<MarsItem>)

data class MarsItem(
    var data: List<MarsItemData>,
    var links: List<NasaItemLinks>
)

data class MarsItemData(
    var nasa_id: String,
    var title: String,
    var date_created: String,
    var center: String,
    var description: String
)

data class NasaItemLinks(var href: String)