package com.savvas.mars.data.di

import com.savvas.mars.core.di.scopes.ApplicationScope
import com.savvas.mars.data.DataSource
import com.savvas.mars.data.RepositoryImp
import com.savvas.mars.data.remote.RemoteDataSource
import com.savvas.mars.domain.Repository
import dagger.Module
import dagger.Provides


@Module(includes = [RemoteDataModule::class])
class DataModule {

    @Provides
    @ApplicationScope
    fun provideRepository(remoteDataSource: RemoteDataSource): Repository =
        RepositoryImp(remoteDataSource)
}