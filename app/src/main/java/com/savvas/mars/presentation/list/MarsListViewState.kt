package com.savvas.mars.presentation.list

import com.savvas.mars.core.presentation.BaseViewState
import com.savvas.mars.domain.MarsList
import com.savvas.mars.domain.error.ErrorEntity


sealed class  MarsListViewState : BaseViewState {

    data class Progress(var isLoading: Boolean) : MarsListViewState()

    data class Success(val data: MarsList) : MarsListViewState()

    data class Failure(val error: ErrorEntity) : MarsListViewState()
}