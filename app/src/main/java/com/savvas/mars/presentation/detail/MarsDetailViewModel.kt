package com.savvas.mars.presentation.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.savvas.mars.core.presentation.BaseViewModel
import com.savvas.mars.domain.GetMarsDetailUseCase
import com.savvas.mars.domain.error.ErrorFactory
import com.savvas.mars.presentation.detail.MarsDetailViewState.*
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo


class MarsDetailViewModel(
    private val getMarsItemUseCase: GetMarsDetailUseCase,
    compositeDisposable: CompositeDisposable,
    errorFactory: ErrorFactory
) : BaseViewModel(compositeDisposable, errorFactory) {

    private val marsDetail: MutableLiveData<MarsDetailViewState> = MutableLiveData()

    fun loadMarsDetail(nasaId: String) =
        getMarsItemUseCase.observe(nasaId)
            .doOnSubscribe { marsDetail.postValue(Progress(true)) }
            .subscribe({
                marsDetail.postValue(Success(it))
            }, {
                marsDetail.postValue(Failure(handleError(it)))
            }).addTo(disposables)


    fun observeMarsDetail() : LiveData<MarsDetailViewState> = marsDetail

}