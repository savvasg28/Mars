package com.savvas.mars.presentation.list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.savvas.mars.core.presentation.BaseViewModel
import com.savvas.mars.domain.GetMarsListUseCase
import com.savvas.mars.domain.error.ErrorFactory
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class MarsListViewModel(
    private val getMarsListUseCase: GetMarsListUseCase,
    private val errorFactory: ErrorFactory,
    compositeDisposable: CompositeDisposable = CompositeDisposable()
) : BaseViewModel(compositeDisposable, errorFactory) {

    private var marsList: MutableLiveData<MarsListViewState> = MutableLiveData()

    fun loadMarsList() {
        getMarsListUseCase.observe(1)
            .doOnSubscribe{marsList.postValue(MarsListViewState.Progress(true))}
            .subscribe({
                marsList.postValue(MarsListViewState.Success(it))
            }, {
                marsList.postValue(MarsListViewState.Failure(handleError(it)))
            }).addTo(disposables)
    }

    fun observeMarsListViewState(): LiveData<MarsListViewState> = marsList

}