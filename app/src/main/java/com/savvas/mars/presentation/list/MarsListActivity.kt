package com.savvas.mars.presentation.list

import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.savvas.mars.R
import com.savvas.mars.core.presentation.BaseActivity
import com.savvas.mars.core.presentation.adapter.GridItemDividerDecoration
import com.savvas.mars.core.utils.setSpan
import com.savvas.mars.domain.entity.MarsEntity
import com.savvas.mars.presentation.detail.MarsDetailActivity
import com.savvas.mars.presentation.list.MarsListViewState.*
import com.savvas.mars.presentation.list.adapter.MarsItemClickListener
import com.savvas.mars.presentation.list.adapter.MarsListAdapter
import com.savvas.mars.presentation.list.di.Injector
import javax.inject.Inject

class MarsListActivity : BaseActivity() {

    override val layoutId = R.layout.activity_main

    @Inject
    lateinit var viewModel: MarsListViewModel
    lateinit var recyclerView: RecyclerView
    private lateinit var marsListAdapter: MarsListAdapter
    private val clickListener: MarsItemClickListener = this::onMarsItemClick

    override fun onCreate(savedInstanceState: Bundle?) {
        Injector.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun bind() {
        setUpRecyclerView()

        seuUpViewModel()
    }

    private fun onMarsItemClick(mars: MarsEntity) {
        startActivity(
            MarsDetailActivity.newInstance(this, mars.id)   //TODO move all to factory method
        )
    }

    private fun seuUpViewModel() {
        viewModel.observeMarsListViewState().observe(this, Observer { viewState ->
            viewState?.let {
                renderViewState(it)
            }
        })

        viewModel.loadMarsList()
    }

    private fun setUpRecyclerView() {
        recyclerView = findViewById(R.id.list)
        with(recyclerView) {
            marsListAdapter = MarsListAdapter(marsItemClick = clickListener)
            adapter = marsListAdapter
            layoutManager = GridLayoutManager(
                applicationContext,
                6
            )
                .setSpan(6) //TODO create something more complex and  it a variable
            setHasFixedSize(true)
            addItemDecoration(
                GridItemDividerDecoration(
                    applicationContext.resources.getDimensionPixelSize(R.dimen.divider_height),
                    ContextCompat.getColor(applicationContext, R.color.colorAccent)
                )
            )

            //TODO add infinite scroll and load data
        }
    }

    private fun renderViewState(viewState: MarsListViewState) {
        when (viewState) {
            is Success -> renderData(viewState.data)
            is Progress -> renderProgress(viewState.isLoading)
            is Failure -> renderError(viewState.error)
        }
    }

    private fun renderData(data: List<MarsEntity>) {
        marsListAdapter.processData(data)
    }

}
