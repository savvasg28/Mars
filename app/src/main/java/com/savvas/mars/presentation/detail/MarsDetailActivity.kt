package com.savvas.mars.presentation.detail

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.savvas.mars.R
import com.savvas.mars.core.presentation.BaseActivity
import com.savvas.mars.domain.entity.MarsEntity
import com.savvas.mars.presentation.detail.MarsDetailViewState.*
import com.savvas.mars.presentation.detail.di.Injector
import timber.log.Timber.d
import javax.inject.Inject

class MarsDetailActivity : BaseActivity() {

    override val layoutId = R.layout.activity_mars_detail

    @Inject
    lateinit var viewModel: MarsDetailViewModel

    private lateinit var marsImageView: ImageView
    private lateinit var marsImageDescription: TextView

    private val nasaId by lazy {
        intent?.getStringExtra(NASA_ID) ?: throw IllegalArgumentException("nasaId is required")
    }

    override fun bind() {
        marsImageView = findViewById(R.id.mars_detail_image)
        marsImageDescription = findViewById(R.id.image_description)

        viewModel.loadMarsDetail(nasaId)

        viewModel.observeMarsDetail().observe(this, Observer { viewState ->
            viewState?.let {
                renderViewState(it)
            }
        })
    }

    private fun renderViewState(viewState: MarsDetailViewState) {
        d(viewState.toString())
        when (viewState) {
            is Success -> renderData(viewState.data)
            is Progress -> renderProgress(viewState.isLoading)
            is Failure -> renderError(viewState.error)
        }
    }

    private fun renderData(data: MarsEntity) {
        with(data) {
            marsImageDescription.text = description
            Glide.with(marsImageView)
                .load(imageSrc)
                .placeholder(R.drawable.ic_launcher_foreground) //TODO provide placeHolder and error screen
                .into(marsImageView)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        Injector.inject(this)
        super.onCreate(savedInstanceState)
    }


    companion object {

        const val NASA_ID = "nasaId"

        fun newInstance(activity: AppCompatActivity, nasaId: String) =
            Intent(activity.applicationContext, MarsDetailActivity::class.java).apply {
                putExtra(NASA_ID, nasaId)
            }

    }
}
