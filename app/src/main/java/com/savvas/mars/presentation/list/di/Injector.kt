package com.savvas.mars.presentation.list.di

import com.savvas.mars.MarsApplication
import com.savvas.mars.core.di.MarsAppComponent
import com.savvas.mars.presentation.list.MarsListActivity


object Injector {

    fun inject(marsListActivity: MarsListActivity) {
        MarsApplication.appComponent
            .getMarsListComponent(MarsListModule(marsListActivity))
            .inject(marsListActivity)
    }

    fun MarsAppComponent.inject(marsListActivity: MarsListActivity) =
        this.getMarsListComponent(MarsListModule(marsListActivity))
            .inject(marsListActivity)

}