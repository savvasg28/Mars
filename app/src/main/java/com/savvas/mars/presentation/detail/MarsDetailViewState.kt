package com.savvas.mars.presentation.detail

import com.savvas.mars.core.presentation.BaseViewState
import com.savvas.mars.domain.entity.MarsEntity
import com.savvas.mars.domain.error.ErrorEntity


sealed class  MarsDetailViewState : BaseViewState {

    data class Progress(var isLoading: Boolean) : MarsDetailViewState()

    data class Success(val data: MarsEntity) : MarsDetailViewState()

    data class Failure(val error: ErrorEntity) : MarsDetailViewState()
}