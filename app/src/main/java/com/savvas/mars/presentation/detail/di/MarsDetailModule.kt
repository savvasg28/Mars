package com.savvas.mars.presentation.detail.di

import androidx.lifecycle.ViewModelProviders
import com.savvas.mars.core.di.scopes.ActivityScope
import com.savvas.mars.core.schedulers.BaseSchedulerProvider
import com.savvas.mars.domain.GetMarsDetailUseCase
import com.savvas.mars.domain.Repository
import com.savvas.mars.presentation.detail.MarsDetailActivity
import com.savvas.mars.presentation.detail.MarsDetailViewModel
import dagger.Module
import dagger.Provides


@Module
class MarsDetailModule(private val marsDetailActivity: MarsDetailActivity) {

    @ActivityScope
    @Provides
    fun provideMarsDetailViewModel(marsDetailViewModelFactory: MarsDetailViewModelFactory) =
        ViewModelProviders.of(marsDetailActivity, marsDetailViewModelFactory).get(
            MarsDetailViewModel::class.java)


    @ActivityScope
    @Provides
    fun provideMarsDetailViewModelFactory(getMarsDetailUseCase: GetMarsDetailUseCase) =
        MarsDetailViewModelFactory(getMarsDetailUseCase)



    @ActivityScope
    @Provides
    fun provideGetMarsDetailUseCase(repository: Repository,
                                  schedulerProvider: BaseSchedulerProvider
    ) =
        GetMarsDetailUseCase(repository,schedulerProvider)
}