package com.savvas.mars.presentation.detail.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.savvas.mars.domain.GetMarsDetailUseCase
import com.savvas.mars.domain.error.ErrorFactory
import com.savvas.mars.presentation.detail.MarsDetailViewModel
import io.reactivex.disposables.CompositeDisposable


class MarsDetailViewModelFactory(private val getMarsDetailUseCase: GetMarsDetailUseCase) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        require(modelClass == MarsDetailViewModel::class.java) { "Unknown ViewModel Class" }
        return MarsDetailViewModel(
            getMarsDetailUseCase,
            CompositeDisposable(),
            ErrorFactory()
        ) as T
    }
}