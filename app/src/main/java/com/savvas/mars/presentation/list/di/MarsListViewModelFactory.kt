package com.savvas.mars.presentation.list.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.savvas.mars.domain.GetMarsListUseCase
import com.savvas.mars.domain.error.ErrorFactory
import com.savvas.mars.presentation.list.MarsListViewModel
import io.reactivex.disposables.CompositeDisposable


class MarsListViewModelFactory(private val getMarsListUseCase: GetMarsListUseCase) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        require(modelClass == MarsListViewModel::class.java) { "Unknown ViewModel Class" }
        return MarsListViewModel(
            getMarsListUseCase,
            ErrorFactory(),
            CompositeDisposable()
        ) as T
    }
}