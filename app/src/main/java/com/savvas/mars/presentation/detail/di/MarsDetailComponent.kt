package com.savvas.mars.presentation.detail.di

import com.savvas.mars.core.di.BaseComponent
import com.savvas.mars.core.di.scopes.ActivityScope
import com.savvas.mars.presentation.detail.MarsDetailActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [MarsDetailModule::class])
interface MarsDetailComponent : BaseComponent<MarsDetailActivity>