package com.savvas.mars.presentation.list.di

import androidx.lifecycle.ViewModelProviders
import com.savvas.mars.core.di.scopes.ActivityScope
import com.savvas.mars.core.schedulers.BaseSchedulerProvider
import com.savvas.mars.domain.GetMarsListUseCase
import com.savvas.mars.domain.Repository
import com.savvas.mars.presentation.list.MarsListActivity
import com.savvas.mars.presentation.list.MarsListViewModel
import dagger.Module
import dagger.Provides


@Module
class MarsListModule(private val marsListActivity: MarsListActivity) {

    @ActivityScope
    @Provides
    fun provideMarsListViewModel(marsListViewModelFactory: MarsListViewModelFactory) =
        ViewModelProviders.of(marsListActivity, marsListViewModelFactory).get(MarsListViewModel::class.java)


    @ActivityScope
    @Provides
    fun provideMarsListViewModelFactory(getMarsListUseCase: GetMarsListUseCase) =
        MarsListViewModelFactory(getMarsListUseCase)



    @ActivityScope
    @Provides
    fun provideGetMarsListUseCase(repository: Repository,
                                  schedulerProvider: BaseSchedulerProvider) =
        GetMarsListUseCase(repository,schedulerProvider)
}