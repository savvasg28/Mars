package com.savvas.mars.presentation.list.di

import com.savvas.mars.core.di.BaseActivityComponent
import com.savvas.mars.core.di.scopes.ActivityScope
import com.savvas.mars.presentation.list.MarsListActivity
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [MarsListModule::class])
interface MarsListComponent : BaseActivityComponent<MarsListActivity>