package com.savvas.mars.presentation.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.savvas.mars.R
import com.savvas.mars.domain.MarsList
import com.savvas.mars.domain.entity.MarsEntity


typealias MarsItemClickListener = (MarsEntity) -> Unit

class MarsListAdapter(
    private var marsList: MarsList = listOf(),
    val marsItemClick: MarsItemClickListener
) :
    RecyclerView.Adapter<MarsListAdapter.MarsListViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MarsListViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_mars, parent, false) as ViewGroup
        val viewHolder = MarsListViewHolder(view)
        view.setOnClickListener { marsItemClick(marsList[viewHolder.adapterPosition]) }
        return viewHolder
    }

    override fun getItemCount(): Int = marsList.size

    override fun onBindViewHolder(holder: MarsListViewHolder, position: Int) {
        holder.bind(marsList[position])

    }

    fun processData(marsList: MarsList) {
        this.marsList = marsList
        notifyDataSetChanged() //TODO Use DiffUtils and payloads
    }


    inner class MarsListViewHolder(itemView: ViewGroup) :
        RecyclerView.ViewHolder(itemView) {

        private val imageView = itemView.findViewById<ImageView>(R.id.mars_image)

        fun bind(marsEntity: MarsEntity) {
            Glide.with(imageView)
                .load(marsEntity.imageSrc)
                .placeholder(R.drawable.ic_launcher_foreground)
                .into(imageView)
        }
    }
}