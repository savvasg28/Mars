package com.savvas.mars.presentation.detail.di

import com.savvas.mars.MarsApplication
import com.savvas.mars.core.di.MarsAppComponent
import com.savvas.mars.presentation.detail.MarsDetailActivity


object Injector {

    fun inject(activity: MarsDetailActivity) {
        MarsApplication.appComponent
            .getMarsDetailComponent(MarsDetailModule(activity))
            .inject(activity)
    }

    fun MarsAppComponent.inject(activity: MarsDetailActivity) = this
        .getMarsDetailComponent(MarsDetailModule(activity))
        .inject(activity)
}