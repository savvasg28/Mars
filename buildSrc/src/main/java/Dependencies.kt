
object Versions {
    const val kotlin = "1.3.50"
    const val min_sdk = 24
    const val target_sdk = 29
    const val compile_sdk = 29
    const val version_code = 1
    const val version_name = "1.0"
    const val android_gradle_plugin = "3.5.0"

    //AndroidX
    const val androidSupportVersion = "29.0.0"
    val appcompatVersion = "1.1.0-alpha05"
    val androidXVersion = "2.2.0-alpha01"
    val legacySupportVersion = "1.0.0"
    val constraintlayoutVersion = "2.0.0-beta1"
    val materialVersion = "1.1.0-alpha07"
    val cardviewVersion = "1.0.0"

    // Dagger
    val daggerVersion = "2.22.1"

    // RxJava
    val rxJavaVersion = "2.2.8"
    val rxKotlinVersion = "2.3.0"
    val rxAndroidVersion = "2.1.1"

    // Network
    val okioVersion = "2.2.2"
    val retrofitVersion = "2.5.0"
    val okhttpVersion = "4.0.0-alpha01"
    val gsonVersion = "2.6.2"


    // Testing
    val mockitoVersion = "2.1.0"
    val mockitoKotlinVersion = "2.0.0"
    val espressoVersion = "3.2.0-beta01"
    val androidArchCoreVersion = "1.1.1"
    val androidXtestVersion = "1.1.0"

    //Persistence
    val roomVersion = "2.2.0-beta01"

    //Glide Image loading
    val glideVersion = "4.9.0"

    // Timber
    val timberVersion = "1.5.1"
}

object Deps {

    val android_gradle_plugin = "com.android.tools.build:gradle:${Versions.android_gradle_plugin}"
    val kotlin_gradle_plugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.kotlin}"

    // Kotlin
    val  kotlin                =  "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.kotlin}"
    val  kotlinExtensions      =  "org.jetbrains.kotlin:kotlin-android-extensions-runtime:${Versions.kotlin}"
    val  kotlinReflect         = "org.jetbrains.kotlin:kotlin-reflect:${Versions.kotlin}"

    // AndroidX
    val  lifecycleCompiler     =  "androidx.lifecycle:lifecycle-compiler:${Versions.androidXVersion}"
    val  lifecycleExtensions   =  "androidx.lifecycle:lifecycle-extensions:${Versions.androidXVersion}"
    val  lifecycleRuntime      =  "androidx.lifecycle:lifecycle-runtime:${Versions.androidXVersion}"
    val  lifecycleLiveData     =  "androidx.lifecycle:lifecycle-livedata-ktx:${Versions.androidXVersion}"

    //AndroidX Design
    val  constraintlayout      =  "androidx.constraintlayout:constraintlayout:${Versions.constraintlayoutVersion}"
    val  material              =  "com.google.android.material:material:${Versions.materialVersion}"
    val  cardview              =  "androidx.cardview:cardview:${Versions.cardviewVersion}"
    val  appCompat             =  "androidx.appcompat:appcompat:${Versions.appcompatVersion}"
    val  designSupport         =  "com.android.support:design:${Versions.androidSupportVersion}"
    val  recyclerView          =  "com.android.support:recyclerview-v7:${Versions.androidSupportVersion}"
    val  design                =  "com.android.support:design:${Versions.androidSupportVersion}"
    val  legacySupport         =  "androidx.legacy:legacy-support-v4:${Versions.legacySupportVersion}"

    // Network
    val  okio                  =  "com.squareup.okio:okio:${Versions.okioVersion}"
    val  okhttp3               =  "com.squareup.okhttp3:okhttp:${Versions.okhttpVersion}"
    val  okhttpLogging         =  "com.squareup.okhttp3:logging-interceptor:${Versions.okhttpVersion}"
    val  retrofit              =  "com.squareup.retrofit2:retrofit:${Versions.retrofitVersion}"
    val  retrofitGsonConverter =  "com.squareup.retrofit2:converter-gson:${Versions.retrofitVersion}"
    val  retrofitRxAdapter     =  "com.squareup.retrofit2:adapter-rxjava2:${Versions.retrofitVersion}"
    val  gson                  =  "com.google.code.gson:gson:${Versions.gsonVersion}"

    //Dagger
    val  dagger                =  "com.google.dagger:dagger:${Versions.daggerVersion}"
    val  daggerCompiler        =  "com.google.dagger:dagger-compiler:${Versions.daggerVersion}"
    val  daggerAndroid         =  "com.google.dagger:dagger-android:${Versions.daggerVersion}"
    val  daggerSupportAndroid  =  "com.google.dagger:dagger-android-support:${Versions.daggerVersion}"
    val  daggerAndroidProcessor=  "com.google.dagger:dagger-android-processor:${Versions.daggerVersion}"

    //RxJava
    val  rxjava                =  "io.reactivex.rxjava2:rxjava:${Versions.rxJavaVersion}"
    val  rxandroid             =  "io.reactivex.rxjava2:rxandroid:${Versions.rxAndroidVersion}"
    val  rxKotlin              =  "io.reactivex.rxjava2:rxkotlin:${Versions.rxKotlinVersion}"

    //Logging
    val  timber                =  "com.github.ajalt:timberkt:${Versions.timberVersion}"

    //Image Loading
    val glide                  = "com.github.bumptech.glide:glide:${Versions.glideVersion}"
    val glideCompiler          = "com.github.bumptech.glide:compiler:${Versions.glideVersion}"

    //Testing
    val  espresso              =  "androidx.test.espresso:espresso-core:${Versions.espressoVersion}"
    val  androidXtestCore      =  "androidx.test:core:${Versions.androidXtestVersion}"
    val  androidXtestRunner    =  "androidx.test:runner:${Versions.androidXtestVersion}"
    val  androidXtestRules     =  "androidx.test:rules:${Versions.androidXtestVersion}"
    val  androidXjUnit         =  "androidx.test.ext:junit:${Versions.androidXtestVersion}"
    val  androidArchCore       =  "android.arch.core:core-testing:${Versions.androidArchCoreVersion}"
    val  roomRuntime           =  "androidx.room:room-runtime:${Versions.roomVersion}"
    val  roomCompiler          =  "androidx.room:room-compiler:${Versions.roomVersion}"
    val  roomRxJava            =  "androidx.room:room-rxjava2:${Versions.roomVersion}"
    val  roomTesting           =  "androidx.room:room-testing:${Versions.roomVersion}"
    val  mockitoCore           =  "org.mockito:mockito-core:${Versions.mockitoVersion}"
    val  okHttpMock            =  "com.squareup.okhttp3:mockwebserver:${Versions.okhttpVersion}"
    val  mockitoKotlin         =  "com.nhaarman.mockitokotlin2:mockito-kotlin:${Versions.mockitoKotlinVersion}"

}
